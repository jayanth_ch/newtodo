const ls = document.querySelector(".list")
const input = document.querySelector(".addTxt")
let todoList = [];

document.addEventListener("keyup", function (event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        addElement()
    }
});

function addElement() {
    const list = document.querySelector(".addTxt").value
    todoList.push({
        text: list,
        complete: false
    })
    input.value = ""
    // console.log(todoList[0].text)
    display();
}

function display() {
    document.querySelector(".list").innerHTML = ""
    for (let i = 0; i < todoList.length; i++)
        document.querySelector(".list").innerHTML += "<div class='data'>" +
            "<input id = 'check' type='checkbox' onclick='toggleStatus(" + i + ")'>" +
            `<span> ${todoList[i].text} </span>` +
            "<button onclick='deleteItem(" + i + ")'>Close</button></div>";

}

function deleteItem(index) {
    console.log(index);
    todoList.splice(index, 1);
    display();
}

function toggleStatus(index) {
    console.log(index);

    todoList[index]['complete'] = !todoList[index]['complete']
    if (todoList[index]['complete'] == true) {

        todoList[index].text = "<s>" + todoList[index].text + "</s>"
        // document.getElementById("check").add(checked)
        // console.log(che)

    } else {
        // const che = !(document.getElementById("check").checked)
        // che = false
        // console.log(che)
        // document.getElementById("check").remove(checked)
        todoList[index].text = todoList[index].text.replace("<s>", "");
        todoList[index].text = todoList[index].text.replace("</s>", "");
    }
    display();
}